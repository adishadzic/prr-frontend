import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import EditIcon from '@material-ui/icons/Edit';
import RequestResourceFormDialog from './RequestResourceFormDialog.js';

const useStyles = makeStyles({
  content: {
    padding: '0px',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    maxWidth: '20ch',
  },
  title: {
    fontSize: 14,
  },
  root: {
    display: 'flex',
    float: 'left',
    marginRight: '5px',
  },
  contentDiv: {
    padding: '2px 5px',
  },
});

export default function ResourceCard({ data }) {
  const [open, setOpen] = React.useState(false);

  const [cardInfo, setCardInfo] = React.useState([data]);

  React.useEffect(() => {
    if (data) {
      setCardInfo(data);
    }
  }, [data]);

  //   const handleCallback = (childData) => {
  //     //updated data from editing
  //     cardInfo = childData;

  const handleCallback = (childData) => {
    //updated data from editing
    //cardInfo = childData;

    //doesnt show EditResourceForm component anymore

    setOpen(false);
    //console.log('cardd:', cardInfo);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const classes = useStyles();

  const renderCard = (card, index) => {
    return (
      <>
        {card && ( //or some dummy condition just for example
          <div>
            <Card className={classes.root} key={index}>
              <CardContent className={classes.content} style={{ padding: '0px' }}>
                <Button size="small" onClick={handleClickOpen}>
                  <div className={classes.contentDiv}>
                    {' '}
                    <EditIcon fontSize="small" color="primary" />{' '}
                  </div>
                  {card.projectRole}
                </Button>
              </CardContent>
            </Card>
            {open ? <RequestResourceFormDialog parentCallback={handleCallback} cardData={card} /> : null}
          </div>
        )}
      </>
    );
  };

  return <div> {cardInfo.length !== 0 && cardInfo?.map(renderCard)}</div>;
}
